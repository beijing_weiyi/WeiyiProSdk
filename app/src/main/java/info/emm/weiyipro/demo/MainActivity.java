package info.emm.weiyipro.demo;

import android.Manifest;
import android.os.Bundle;

import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import info.emm.weiyicloud.WySdk;
import info.emm.weiyicloud.user.RoleType;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    private String serial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView roomNum = findViewById(R.id.room_num);
        View join = findViewById(R.id.join);
        final String trim = roomNum.getText().toString().trim();
        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                serial = trim;//会议id
                requestPermission();
            }
        });
    }

    private static final int WY_REQUEST_CODE = 100;

    private void requestPermission() {
        String[] permissions = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.MODIFY_AUDIO_SETTINGS,
                Manifest.permission.READ_EXTERNAL_STORAGE};

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    permission) != PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        permissions,
                        WY_REQUEST_CODE);
                return;
            }
        }

        join();


    }

    private void join() {
//        /**
//         * *带界面 入会议
//         * {@link RoleType.Type}
//         */
//        WySdk.getInstance().joinMeeting(MainActivity.this,
//                "4123",//用户id
//                "test1", //昵称
//                "578785594", //会议号
//                null, //sig，邀请入会参数，默认null
//                RoleType.PRESENTER//普通用户 ，RoleType.STRALTH 隐身用户
//        );


        /**
         *带hd界面 入会议
         * {@link RoleType.Type}
         */
        WySdk.getInstance().joinHDMeeting(MainActivity.this,
                "4123",//用户id
                "test1", //昵称
                "205244634", //会议号
                RoleType.PRESENTER//普通用户 ，RoleType.STRALTH 隐身用户
        );
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == WY_REQUEST_CODE
                && grantResults.length == 6
                && grantResults[0] == PERMISSION_GRANTED
                && grantResults[1] == PERMISSION_GRANTED
                && grantResults[2] == PERMISSION_GRANTED
                && grantResults[3] == PERMISSION_GRANTED
                && grantResults[4] == PERMISSION_GRANTED
                && grantResults[5] == PERMISSION_GRANTED) {

            join();
        }
    }
}
