package info.emm.weiyipro.demo;


import android.app.Application;

import info.emm.weiyicloud.WySdk;


public class WeiyiApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();


        WySdk.getInstance().setHasChirman(false);//移动端暂时不允许申请主席
        WySdk.getInstance().setDebugModel(false);//视频参数开关
        WySdk.getInstance().setNotificationIcon(R.mipmap.ic_launcher);//设置sdk logo
        WySdk.getInstance().init(this, "https://test.weiyipro.com");//url后面不要带"/"

        //HDMeeting界面生效功能
        WySdk.getInstance().hideUSBCapture(true);//隐藏外接usb

    }

}
