# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


-keep class info.emm.weiyicloud.model.** { *; }
-keep class info.emm.weiyicloud.v2.model.** { *; }
-keep class info.emm.weiyicloud.user.** { *; }
-keep class info.emm.weiyicloud.base.** { *; }
-keep class info.emm.commonlib.http.bean.** { *; }


-keep class com.allenliu.versionchecklib.** { *; }
-dontwarn com.allenliu.versionchecklib.**

-keep class org.jboss.netty.** { *; }
-keep  interface org.jboss.netty.**{ *; }
-dontwarn org.jboss.netty.**

-keep public class * implements  com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}
-keep class com.bumptech.** {
    *;
}
-keep class okhttp3.** { *; }
-dontwarn okhttp3.**
-keep class rx.** { *; }
-dontwarn rx.**
-keep class retrofit2.** { *; }
-dontwarn retrofit2.**
-keep class rx.** { *; }
-dontwarn rx.**


-keep class net.sqlcipher.**{*;}
#greendao 3
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
    public static java.lang.String TABLENAME;
}
-keep class **$Properties

-keepattributes *Annotation*
-keepclassmembers class * {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

#https 证书混淆问题
-keep public class android.net.http.SslError
-dontwarn android.webkit.WebView
-dontwarn android.net.http.SslError

-dontwarn Android.webkit.WebViewClient
-keep class android.webkit.**{*;}
-dontwarn android.webkit.**
-keep class org.webrtc.**{*;}
-dontwarn org.webrtc.**