# WeiyiProSdk

#### 介绍
微议pro sdk 
基于webrtc的多人实时音视频互动sdk

视频编解码支持vp8,vp9,h264,视频分辨率默认1280x720@25,带宽和编解码能力达到的情况下支持1920x1080@30

音频编解码支持常见opus,aac等,支持AEC,NS,

#### 运行环境
1.建议Android Studio 4.3.1及以上,如需使用低版本Android Studio ,请确保Android Studio版本高于3.0.1,并自行修改到对应的gradle版本
2.minSdkVersion 21 : 录屏要求系统最低api21
#### 权限

   
```
 <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
```



#### 依赖

1.app的 libs文件添加

```
weiyimeeting-release.aar
```
2.
```
 implementation fileTree(dir: "libs", include: ["*.jar","*.aar"])//依赖所有.aar
 implementation('io.socket:socket.io-client:1.0.0') {
        // excluding org.json which is provided by Android
        exclude group: 'org.json', module: 'json'
    }

    implementation 'io.reactivex.rxjava2:rxandroid:2.0.1'
    implementation 'io.reactivex.rxjava2:rxjava:2.1.16'
    implementation 'com.squareup.retrofit2:retrofit:2.2.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.2.0'
    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.2.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.8.1'
    implementation 'com.google.code.gson:gson:2.8.2'
    implementation 'com.squareup.retrofit2:converter-scalars:2.0.0'

    implementation 'com.king.zxing:zxing-lite:1.1.9'
    implementation 'com.android.support:recyclerview-v7:28.0.0}'
```

 **注意:** 
本项目使用Androidx 
 gradle.properties 文件里面需添加
```
android.useAndroidX=true
android.enableJetifier=true
```



#### 使用

1. 初始化
继承 info.emm.commonlib.base.BaseApplication;

```
public class WeiyiApplication extends Application;{


    @Override
    public void onCreate() {
        super.onCreate();
         WySdk.getInstance().setHasChirman(false);//移动端暂时不允许申请主席
         WySdk.getInstance().setDebugModel(false);//视频参数开关
         WySdk.getInstance().setNotificationIcon(R.mipmap.ic_launcher);//设置sdk logo
         WySdk.getInstance().init(this, "https://test.weiyipro.com");//url后面不要带"/"

         //HDMeeting界面生效功能
         WySdk.getInstance().hideUSBCapture(true);//隐藏外接usb
    }

}
```


2.进入会议

#### 普通会议界面
```
 /**
  * 进入会议
  * @param activity 当前activity
  * @param userId  用户id
  * @param nickName 昵称
  * @param serial 会议号
  * @param sig 会议验证，没有填null
  * @param role 用户入会角色，{@link RoleType.Type} 注：移动端暂时没主席角色
  */
public void joinMeeting(AppCompatActivity activity, String userId, String nickName, String serial, String sig,String role)

```

#### HD会议界面

```
 /**
  *带hd界面 入会议
  * {@link RoleType.Type}
  */
WySdk.getInstance().joinHDMeeting(MainActivity.this,
                "4123",//用户id
                "test1", //昵称
                "205244634", //会议号
                RoleType.PRESENTER//普通用户 ，RoleType.STRALTH 隐身用户
        );
```


#### demo
https://gitee.com/beijing_weiyi/WeiyiProSdk



#### 网站
标准版:http://www.weiyicloud.com

升级版: https://www.weiyipro.com

邮箱:zuoml@weiyicloud.com